package be.bjarno.lagomtest.hello.impl;

import com.lightbend.lagom.javadsl.persistence.AggregateEventTag;

/**
 * Created by bjarno on 04/05/17.
 */
public class HelloEventTag {
    public static final AggregateEventTag<HelloEvent> INSTANCE = AggregateEventTag.of(HelloEvent.class);
}
