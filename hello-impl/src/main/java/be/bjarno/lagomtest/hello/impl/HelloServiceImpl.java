/*
 * Copyright (C) 2016-2017 Lightbend Inc. <https://www.lightbend.com>
 */
package be.bjarno.lagomtest.hello.impl;

import akka.Done;
import akka.NotUsed;
import be.bjarno.lagomtest.hello.api.*;
import be.bjarno.lagomtest.hello.impl.HelloCommand.Hello;
import com.lightbend.lagom.javadsl.api.ServiceCall;
import com.lightbend.lagom.javadsl.persistence.PersistentEntityRef;
import com.lightbend.lagom.javadsl.persistence.PersistentEntityRegistry;

import java.util.List;
import java.util.Optional;
import java.util.concurrent.CompletionStage;
import java.util.concurrent.ExecutionException;
import java.util.stream.Collectors;

import javax.inject.Inject;

import be.bjarno.lagomtest.hello.impl.HelloCommand.*;
import com.lightbend.lagom.javadsl.persistence.ReadSide;
import com.lightbend.lagom.javadsl.persistence.cassandra.CassandraSession;
import org.pcollections.PSequence;
import org.pcollections.TreePVector;

/**
 * Implementation of the HelloService.
 */
public class HelloServiceImpl implements HelloService {

  private final PersistentEntityRegistry persistentEntityRegistry;
  private final CassandraSession db;
  private final ReadSide readSide;

  @Inject
  public HelloServiceImpl(PersistentEntityRegistry persistentEntityRegistry, CassandraSession db, ReadSide readSide) {
    this.persistentEntityRegistry = persistentEntityRegistry;
    this.db = db;
    this.readSide = readSide;

    persistentEntityRegistry.register(HelloEntity.class);
    readSide.register(HelloEventProcessor.class);
  }

  @Override
  public ServiceCall<NotUsed, String> hello(String id) {
    return request -> {
      // Look up the hello world entity for the given ID.
      PersistentEntityRef<HelloCommand> ref = persistentEntityRegistry.refFor(HelloEntity.class, id);
      // Ask the entity the Hello command.
      return ref.ask(new Hello(id, Optional.empty()));
    };
  }

  @Override
  public ServiceCall<GreetingMessage, Done> useGreeting(String id) {
    return request -> {
      // Look up the hello world entity for the given ID.
      PersistentEntityRef<HelloCommand> ref = persistentEntityRegistry.refFor(HelloEntity.class, id);
      // Tell the entity to use the greeting message specified.
      return ref.ask(new UseGreetingMessage(id, request.message));
    };

  }

  @Override
  public ServiceCall<NotUsed, PSequence<be.bjarno.lagomtest.hello.api.Hello>> allGreetings() {
    return req -> {
      CompletionStage<PSequence<be.bjarno.lagomtest.hello.api.Hello>> result =
              db.selectAll("SELECT * FROM hello")
                      .thenApply(rows -> {
                        List<be.bjarno.lagomtest.hello.api.Hello> boards = rows.stream()
                                .map(row -> {
                                  String id = row.getString("helloID");
                                  CompletionStage<String> stage = entityRef(id).ask(GetHelloString.INSTANCE);
                                  try {
                                    return new be.bjarno.lagomtest.hello.api.Hello(id, stage.toCompletableFuture().get());
                                  } catch (InterruptedException | ExecutionException e) {
                                    e.printStackTrace();
                                    return null;
                                  }
                                })
                                .filter(hello -> (hello != null))
                                .collect(Collectors.toList());
                        return TreePVector.from(boards);
                      });
      return result;
    };
  }

  private PersistentEntityRef<HelloCommand> entityRef(String id) {
    return persistentEntityRegistry.refFor(HelloEntity.class, id);
  }

}
