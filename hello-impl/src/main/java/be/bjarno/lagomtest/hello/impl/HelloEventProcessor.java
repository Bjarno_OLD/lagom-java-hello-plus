package be.bjarno.lagomtest.hello.impl;

import akka.Done;
import com.datastax.driver.core.BoundStatement;
import com.datastax.driver.core.PreparedStatement;
import com.lightbend.lagom.javadsl.persistence.AggregateEventTag;
import com.lightbend.lagom.javadsl.persistence.ReadSideProcessor;
import com.lightbend.lagom.javadsl.persistence.cassandra.CassandraReadSide;
import com.lightbend.lagom.javadsl.persistence.cassandra.CassandraSession;
import org.pcollections.PSequence;
import org.pcollections.TreePVector;

import javax.inject.Inject;
import java.util.List;
import java.util.concurrent.CompletionStage;

import static com.lightbend.lagom.javadsl.persistence.cassandra.CassandraReadSide.completedStatement;

/**
 * Created by bjarno on 04/05/17.
 */
public class HelloEventProcessor extends ReadSideProcessor<HelloEvent> {
    private final CassandraSession session;
    private final CassandraReadSide readSide;
    private PreparedStatement writeBoards = null;

    @Inject
    public HelloEventProcessor(CassandraSession session, CassandraReadSide readSide) {
        this.session = session;
        this.readSide = readSide;
    }

    @Override
    public PSequence<AggregateEventTag<HelloEvent>> aggregateTags() {
        return TreePVector.singleton(HelloEventTag.INSTANCE);

    }

    @Override
    public ReadSideProcessor.ReadSideHandler<HelloEvent> buildHandler() {
        return readSide.<HelloEvent>builder("hello_offset")
                .setGlobalPrepare(this::prepareCreateTables)
                .setPrepare((ignored) -> prepareWriteFollowers())
                .setEventHandler(HelloEvent.GreetingMessageChanged.class, this::processHelloAdded)
                .build();
    }

    private void setWriteBoards(PreparedStatement writeBoards) {
        this.writeBoards = writeBoards;
    }

    private CompletionStage<Done> prepareWriteFollowers() {
        return session.prepare("INSERT INTO hello (helloID) VALUES (?)").thenApply(ps -> {
            setWriteBoards(ps);
            return Done.getInstance();
        });
    }


    private CompletionStage<Done> prepareCreateTables() {
        return session.executeCreateTable(
                "CREATE TABLE IF NOT EXISTS hello ("
                        + "helloID text,"
                        + "PRIMARY KEY (helloID))");
    }

    private CompletionStage<List<BoundStatement>> processHelloAdded(HelloEvent.GreetingMessageChanged event) {
        BoundStatement bindWriteBoards = writeBoards.bind();
        bindWriteBoards.setString("helloID", event.id);
        return completedStatement(bindWriteBoards);
    }

}
