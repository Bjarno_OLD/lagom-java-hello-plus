# Hello Lagom
This is a Maven project for the Lagom Framework (https://www.lagomframework.com/). It's almost the same default project, but with the following changes...

- There is a method to list all saved greeting messages, by using CassandraReadSide

## Additional examples that can be added later

- Inter-service communication


## Why this project?

I wanted to test whether some bug occured as well in a minimal project. This is the result of that after some cleaning.
