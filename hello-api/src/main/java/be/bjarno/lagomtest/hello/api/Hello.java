package be.bjarno.lagomtest.hello.api;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import javax.annotation.concurrent.Immutable;

/**
 * Created by bjarno on 04/05/17.
 */

@Immutable
@JsonSerialize
public class Hello {

    public final String id;
    public final String message;

    @JsonCreator
    public Hello(String id, String message) {
        this.id = id;
        this.message = message;
    }

    @Override
    public String toString() {
        return "Hello{" +
                "id='" + id + '\'' +
                ", message='" + message + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Hello hello = (Hello) o;

        if (id != null ? !id.equals(hello.id) : hello.id != null) return false;
        return message != null ? message.equals(hello.message) : hello.message == null;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (message != null ? message.hashCode() : 0);
        return result;
    }
}
